package com.xyz.alihelper.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

internal class ViewPagerAdapter(manager: FragmentManager, val fragments: List<Fragment>, val titles: List<String>) :
    FragmentPagerAdapter(manager) {

  override fun getItem(position: Int) = fragments[position]

  override fun getCount() = fragments.size

  override fun getPageTitle(position: Int) = titles[position]
}