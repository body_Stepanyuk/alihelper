package com.xyz.alihelper.widget

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.widget.FrameLayout
import com.xyz.alihelper.R
import kotlinx.android.synthetic.main.progress_view_layout.view.*

/**
 * Created by bogdanstepanyuk on 23.04.17.
 */

class ProgressView : FrameLayout {

    constructor(context: Context?) : super(context) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    fun init(context: Context?) {
        inflate(context, R.layout.progress_view_layout, this)

        pieView.setInnerBackgroundColor(Color.WHITE)
        pieView.setTextColor(Color.parseColor(context?.getString(R.color.green)))
        pieView.setPercentageBackgroundColor(Color.parseColor(context?.getString(R.color.green)))
    }
}