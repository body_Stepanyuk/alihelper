package com.xyz.qrscaner.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.xyz.alihelper.R
import com.xyz.alihelper.models.Product

/**
 * Created by bogdan on 14.04.17.
 */
class ProductView : FrameLayout {

  constructor(context: Context?) : super(context) {
    init(context)
  }

  constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
    init(context)
  }

  constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
    init(context)
  }

  fun init(context: Context?) {
    inflate(context, R.layout.product_view_layout, this)
  }

  fun setData(product: Product) {
//    text.text = product.title
//    progressBar.visibility = View.VISIBLE
//    Glide.with(context).load(product.images).listener(object : RequestListener<String, GlideDrawable> {
//      override fun onException(e: Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
//        return false
//      }
//
//      override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
//        progressBar.visibility = View.GONE
//        return false
//      }
//
//    }).diskCacheStrategy(DiskCacheStrategy.ALL).into(image)
  }
}