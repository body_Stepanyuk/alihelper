package com.xyz.alihelper.ui.activities

import android.os.Bundle
import android.view.Menu
import com.xyz.alihelper.R
import com.xyz.alihelper.adapters.ViewPagerAdapter
import com.xyz.alihelper.ui.fragments.DinamicFragment
import com.xyz.alihelper.ui.fragments.RatingFragment
import kotlinx.android.synthetic.main.product_activity_layout.*

/**
 * Created by bogdanstepanyuk on 23.04.17.
 */
class ProductActivity : BaseActivity() {

  override fun contentView() = R.layout.product_activity_layout

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setSupportActionBar(product_toolbar)
    with(supportActionBar!!) {
      setHomeButtonEnabled(true)
      setDisplayHomeAsUpEnabled(true)
      setDisplayShowHomeEnabled(false)
    }

    supportActionBar?.title = "Проверка результата"
    product_toolbar.setNavigationOnClickListener { finish() }

    product_view_pager.adapter = ViewPagerAdapter(supportFragmentManager, listOf(RatingFragment(), DinamicFragment()),
        listOf("Рейтинг продаца", "Динамика цены"))
    product_tabs.setupWithViewPager(product_view_pager)
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.product_menu, menu)
    return true
  }
}