package com.xyz.alihelper.ui.fragments

import android.os.Bundle
import android.view.View
import com.xyz.alihelper.R

/**
 * Created by bogdan on 13.04.17.
 */
class AboutFragment: BaseFragment() {

  override fun contentView() = R.layout.about_fragment_layout

  override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
  }
}