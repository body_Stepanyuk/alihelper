package com.xyz.alihelper.ui.fragments

import android.os.Bundle
import android.view.View
import com.xyz.alihelper.R
import com.xyz.alihelper.ui.activities.ProductActivity
import kotlinx.android.synthetic.main.favorite_fragment_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.support.v4.startActivity

/**
 * Created by bogdan on 16.04.17.
 */
class FavoriteFragment : BaseFragment() {

  override fun contentView() = R.layout.favorite_fragment_layout

  override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    open_ali_express.onClick {
      startActivity<ProductActivity>()
    }
  }

}