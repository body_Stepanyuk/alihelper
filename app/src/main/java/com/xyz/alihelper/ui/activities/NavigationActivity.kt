package com.xyz.alihelper.ui.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import com.xyz.alihelper.R
import com.xyz.alihelper.adapters.ViewPagerAdapter
import com.xyz.alihelper.ui.fragments.*
import kotlinx.android.synthetic.main.drawer_layout.*

/**
 * Created by bogdan on 12.04.17.
 */
class NavigationActivity : BaseActivity(), OnNavigationItemSelectedListener {

  companion object constants {

    //home page
    val HISTORY = "history"
    val SHARE = "share"
    val INSTRUCTION = "instruction"
    val SETTINGS = "settings"
    val SUPPORT = "support"
    val ABOUT = "about"
  }

  val fragments = mapOf(
      HISTORY to HistoryFragment(),
      SHARE to ShareFragment(),
      INSTRUCTION to InstructionFragment(),
      SETTINGS to SettingsFragment(),
      SUPPORT to SupportFragment(),
      ABOUT to AboutFragment())

  override fun contentView() = R.layout.drawer_layout

  @SuppressLint("NewApi")
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setSupportActionBar(navigation_toolbar)
    with(supportActionBar!!) {
      setHomeButtonEnabled(true)
      setDisplayHomeAsUpEnabled(true)
      setDisplayShowHomeEnabled(false)
    }

    val s = intent.getStringExtra(Intent.EXTRA_TEXT)

    viewpager.adapter = ViewPagerAdapter(supportFragmentManager, listOf(FavoriteFragment(), ProductsFragment()), listOf("Favorite", "All products"))
    tabs.setupWithViewPager(viewpager)

    navigation_toolbar.setNavigationOnClickListener { }

    ActionBarDrawerToggle(this, drawer, navigation_toolbar,
        R.string.navigation_drawer_open, R.string.navigation_drawer_close).syncState()
    drawer.setStatusBarBackgroundColor(Color.parseColor(getString(R.color.black)))
    nav_view?.setNavigationItemSelectedListener(this)

    replaceFragment(R.string.history, fragments[HISTORY])
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.item_history -> {
        replaceFragment(R.string.history, fragments[HISTORY])
        toggleTabsVisible(true)
      }
      R.id.item_share -> {
        replaceFragment(R.string.share, fragments[SHARE])
        toggleTabsVisible(false)
      }
      R.id.item_instruction -> {
        replaceFragment(R.string.instruction, fragments[INSTRUCTION])
        toggleTabsVisible(false)
      }
      R.id.item_settings -> {
        replaceFragment(R.string.settings, fragments[SETTINGS])
        toggleTabsVisible(false)
      }
      R.id.item_support -> {
        replaceFragment(R.string.support, fragments[SUPPORT])
        toggleTabsVisible(false)
      }
      R.id.item_about -> {
        replaceFragment(R.string.about, fragments[ABOUT])
        toggleTabsVisible(false)
      }
    }
    drawer?.closeDrawer(GravityCompat.START)
    return true
  }

  private fun replaceFragment(title: Int, fragment: Fragment?) {
    supportActionBar?.title = getString(title)
    fragment?.let {
      supportFragmentManager.beginTransaction().replace(R.id.container_fragments, it).commit()
    }
  }

  override fun onBackPressed() {
    if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START)
    }

    if (supportFragmentManager.findFragmentById(R.id.container_fragments) !is HistoryFragment) {
      replaceFragment(R.string.history, fragments[HISTORY])
      toggleTabsVisible(true)
      drawer?.post { setValidMenuItemChecked() }
    } else finish()
  }

  fun setValidMenuItemChecked() {
    fun setCheckedItem(id: Int) {
      nav_view.setCheckedItem(nav_view.menu.findItem(id).itemId)
    }

    when (supportFragmentManager.findFragmentById(R.id.container_fragments)) {
      is SettingsFragment -> setCheckedItem(R.id.item_instruction)
    }
  }

  fun toggleTabsVisible(tabsVisible: Boolean) {
    tabs_container.visibility = if (tabsVisible) VISIBLE else GONE
    container_fragments.visibility = if (tabsVisible) GONE else VISIBLE
  }
}
