package com.xyz.alihelper.ui.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.xyz.alihelper.R
import kotlinx.android.synthetic.main.dinamic_fragment_layout.*
import java.util.*

/**
 * Created by bogdanstepanyuk on 23.04.17.
 */
class DinamicFragment : BaseFragment() {

  override fun contentView() = R.layout.dinamic_fragment_layout

  private var vals = floatArrayOf(23.5f, 25.2f, 24.8f, 27.5f, 28.0f, 27.5f, 24.5f, 25.0f)
  private var labels = hashMapOf(0f to "19.02", 1f to "20.02", 2f to "21.02", 3f to "22.02",
      4f to "23.02", 5f to "24.02", 6f to "25.02", 7f to "26.02")

  override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val lineData = getData()

    with(line_chart) {
      setDescription("")
      setTouchEnabled(false)
      isDragEnabled = true
      setScaleEnabled(true)
      setPinchZoom(true)
      setBackgroundColor(Color.rgb(235,242,208))
      setViewPortOffsets(60f, 0f, 60f, 50f)

      setTouchEnabled(true)
      isDragEnabled = true
      setScaleEnabled(true)
      setPinchZoom(true)

      axisRight.setDrawAxisLine(false)
      axisLeft.setAxisMaxValue(vals[vals.size - 1] + 7)
      axisLeft.setAxisMinValue(vals[0] - 2)
      axisRight.setDrawGridLines(false)
      xAxis.setDrawAxisLine(true)
      xAxis.setDrawGridLines(false)

      axisLeft.isEnabled = false
      axisRight.isEnabled = false
      axisLeft.spaceTop = 40f
      axisRight.isEnabled = false
      xAxis.isEnabled = true
      xAxis.textSize = 10f
      xAxis.yOffset = -10f
      xAxis.position = XAxis.XAxisPosition.BOTTOM_INSIDE
      xAxis.setDrawAxisLine(false)
      xAxis.mLabelHeight = 10


//      xAxis.valueFormatter = object : AxisValueFormatter {
//        override fun getFormattedValue(value: Float, axis: AxisBase?) = labels.getValue(value)
//
//        override fun getDecimalDigits() = 0
//      }

      data = lineData
      val l = legend
      l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
      l.mNeededHeight = 50f
      l.setDrawInside(false)
    }
  }

  private fun getData(): LineData {
    val yVals = ArrayList<Entry>()
    val sets = arrayListOf<ILineDataSet>()
    vals.mapIndexed { index, fl ->
      yVals.add(Entry(index.toFloat(), fl))
      sets.add(getDashedLine(index.toFloat(), fl))
    }

    val lineDataSet = LineDataSet(yVals, "")
    lineDataSet.setDrawValues(true)
    lineDataSet.lineWidth = 2f
    lineDataSet.circleRadius = 7f
    lineDataSet.circleHoleRadius = 3.5f
    lineDataSet.setDrawCircleHole(true)
    lineDataSet.setCircleColorHole(Color.rgb(235,242,208))
    lineDataSet.setCircleColors(intArrayOf(Color.rgb(63,157,53), Color.rgb(186,76,42),
        Color.rgb(63,157,53), Color.rgb(186,76,42), Color.rgb(186,76,42),
        Color.rgb(63,157,53), Color.rgb(63,157,53), Color.rgb(186,76,42)))
    lineDataSet.setValueFormatter { value, entry, dataSetIndex, viewPortHandler -> "$value$" }
    lineDataSet.color = Color.rgb(177,200,136)
    lineDataSet.highLightColor = Color.WHITE
    lineDataSet.valueTextColor = Color.BLACK
    lineDataSet.valueTextSize = 12f

    sets.add(lineDataSet)

    return LineData(sets)
  }

  private fun getDashedLine(x: Float, y: Float): LineDataSet {
    val lineDataSet = LineDataSet(listOf(Entry(x, 0f), Entry(x, y)), "")
    lineDataSet.setDrawCircles(false)
    lineDataSet.lineWidth = 2f
    lineDataSet.circleRadius = 1f
    lineDataSet.circleHoleRadius = 0f
    lineDataSet.enableDashedLine(10f, 10f, 0f)
    lineDataSet.color = Color.rgb(216,232,169)
    lineDataSet.setDrawValues(false)

    return lineDataSet
  }
}