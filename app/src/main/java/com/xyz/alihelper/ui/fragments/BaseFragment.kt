package com.xyz.alihelper.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.voter.xyz.config.AppPreferences
import com.xyz.alihelper.AliHelperApplication
import javax.inject.Inject

/**
 * Created by bogdan on 13.04.17.
 */
abstract class BaseFragment : Fragment() {

  @Inject
  lateinit var prefs: AppPreferences

  abstract fun contentView(): Int

  override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater!!.inflate(contentView(), container, false)
  }

  override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    AliHelperApplication.getComponent().inject(this)
  }
}