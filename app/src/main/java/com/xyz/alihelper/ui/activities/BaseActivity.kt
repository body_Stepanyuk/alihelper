package com.xyz.alihelper.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import app.voter.xyz.config.AppPreferences
import com.xyz.alihelper.AliHelperApplication
import com.xyz.alihelper.network.ApiService
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

  @Inject
  lateinit var prefs: AppPreferences

  @Inject
  lateinit var apiService: ApiService

  abstract fun contentView(): Int

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(contentView())
    AliHelperApplication.getComponent().inject(this)
  }
}
