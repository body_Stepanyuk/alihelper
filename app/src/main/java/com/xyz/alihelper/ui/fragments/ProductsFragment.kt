package com.xyz.alihelper.ui.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.xyz.alihelper.R
import kotlinx.android.synthetic.main.products_fragment_layout.*
import com.jjoe64.graphview.series.LineGraphSeries
import com.jjoe64.graphview.series.DataPoint
import android.graphics.DashPathEffect
import android.graphics.Paint
import kotlinx.android.synthetic.main.progress_view_layout.view.*


/**
 * Created by bogdan on 16.04.17.
 */
class ProductsFragment : BaseFragment() {

    override fun contentView() = R.layout.products_fragment_layout

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val series = LineGraphSeries(arrayOf(DataPoint(0.toDouble(), 1.toDouble()), DataPoint(1.toDouble(), 5.toDouble())
                , DataPoint(2.toDouble(), 3.toDouble())))

//        series.title = "Random Curve 1"
//        series.color = Color.GREEN
//        series.isDrawDataPoints = true
//        series.dataPointsRadius = 10f
//        series.thickness = 8


        val paint = Paint()
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 10f
        paint.pathEffect = DashPathEffect(floatArrayOf(8f, 5f), 0f)
        series.setCustomPaint(paint)

        chart.addSeries(series)
    }

}