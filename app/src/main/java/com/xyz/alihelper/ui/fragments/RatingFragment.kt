package com.xyz.alihelper.ui.fragments

import android.os.Bundle
import android.view.View
import com.xyz.alihelper.R

/**
 * Created by bogdanstepanyuk on 23.04.17.
 */
class RatingFragment : BaseFragment() {

    override fun contentView() = R.layout.rating_fragment_layout

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}