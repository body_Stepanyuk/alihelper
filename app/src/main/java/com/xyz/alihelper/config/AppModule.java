package com.xyz.alihelper.config;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xyz.alihelper.network.ApiService;

import javax.inject.Singleton;

import app.voter.xyz.config.AppPreferences;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    AppPreferences providePrefs() {
        return new AppPreferences(context);
    }

    @Provides
    @Singleton
    ApiService provideApiService() {
        return new ApiService();
    }

    @Singleton
    @Provides
    public Gson provideGson() {
        return new GsonBuilder().create();
    }

//  @Singleton
//  @Provides
//  public API provideApi(Gson gson) {
//    return new Retrofit.Builder()
////        .baseUrl("base url")
//        .addConverterFactory(GsonConverterFactory.create(gson))
//        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//        .build()
//        .create(API.class);
//  }

}
