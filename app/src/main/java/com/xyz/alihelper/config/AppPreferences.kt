package app.voter.xyz.config

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class AppPreferences(context: Context) {
  private val prefs: SharedPreferences

  init {
    prefs = PreferenceManager.getDefaultSharedPreferences(context)
  }

  fun shouldUseVotes() = prefs.getBoolean("use_votes", true)

  fun setShouldUseVotes(useVotes: Boolean) = prefs.edit().putBoolean("use_votes", useVotes).apply()

  fun setHasLatestResults(b: Boolean) = prefs.edit().putBoolean("has_latest_results", b).apply()

  fun locationPermissionWasShow(b: Boolean) = prefs.edit().putBoolean("first_start", b).apply()

  fun locationPermissionWasShow() = prefs.getBoolean("first_start", false)

  fun getHasLatestResults() = prefs.getBoolean("has_latest_results", false)

  fun setLocation(location: String) = prefs.edit().putString("location", location).apply()

  fun getLocation(): String? = prefs.getString("location", null)
  fun setEcon(econ: Float) = prefs.edit().putFloat("econ", econ.toFloat()).apply()
  fun getEcon() = prefs.getFloat("econ", 0f)

  fun setSocial(econ: Float) = prefs.edit().putFloat("social", econ.toFloat()).apply()
  fun getSocial() = prefs.getFloat("social", 0f)


  fun setFbName(it: String) = prefs.edit().putString("fb_name", it).apply()

  fun getFbName(): String? = prefs.getString("fb_name", null)

  fun setFbEmail(it: String) = prefs.edit().putString("fb_email", it).apply()

  fun getFbEmail(): String? = prefs.getString("fb_email", null)

  fun setFbGender(it: String) = prefs.edit().putString("fb_gender", it).apply()

  fun getFbGender(): String? = prefs.getString("fb_gender", null)

  fun setFbCity(it: String) = prefs.edit().putString("fb_city", it).apply()

  fun getFbCity(): String? = prefs.getString("fb_city", null)

  fun setFbMinAge(it: Int) = prefs.edit().putInt("fb_min_age", it).apply()

  fun getFbMinAge(): Int? {
    val age = prefs.getInt("fb_min_age", 0)
    return if (age != 0) age else null
  }

  fun setIsTutorialCompleted(isTutorialCompleted: Boolean) = prefs.edit().putBoolean("is_tutorial_completed", isTutorialCompleted).apply()

  fun isTutorialCompleted() = prefs.getBoolean("is_tutorial_completed", false)
}
