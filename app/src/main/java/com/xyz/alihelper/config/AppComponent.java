package com.xyz.alihelper.config;

import com.xyz.alihelper.ui.activities.BaseActivity;
import com.xyz.alihelper.ui.fragments.BaseFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
    AppModule.class
})
public interface AppComponent {
  void inject(BaseActivity baseActivity);

  void inject(BaseFragment baseFragment);
}
