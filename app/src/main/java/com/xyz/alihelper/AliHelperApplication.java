package com.xyz.alihelper;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.xyz.alihelper.config.AppComponent;
import com.xyz.alihelper.config.AppModule;
import com.xyz.alihelper.config.DaggerAppComponent;

public class AliHelperApplication extends Application {

    private static AppComponent component;

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}